package persistence;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserProductPersister extends DataAccess {
    public void insert(UserProduct userProduct1) {
        Transaction tx = null;
        try (Session session = factory.openSession();) {
            tx = session.beginTransaction();
            session.save(userProduct1);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }}
//    public void select(int id) {
//       List<UserProduct> userProducts = null;
//       List<Product> products=null;
//
//        Transaction tx = null;
//
//        try(Session session = factory.openSession();) {
//            tx = session.beginTransaction();
//            CriteriaBuilder builder = session.getCriteriaBuilder();
//            CriteriaQuery<UserProduct> query = builder.createQuery(UserProduct.class);
//            Root<UserProduct> root = query.from(UserProduct.class);
//            query.select(root).where(builder.equal(root.get("id"), id));
//            userProducts = session.createQuery(query).getResultList();
//            for (UserProduct userProduct:userProducts
//                 ) {
//
//
//            }
//            products=userProducts.
//
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) tx.rollback();
//            e.printStackTrace();
//        }



