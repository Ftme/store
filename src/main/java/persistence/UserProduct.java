package persistence;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class UserProduct {

    @EmbeddedId
    private UserProductId id;

    @ManyToOne
    @JoinColumn(name = "fk_user", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "fk_product", insertable = false, updatable = false)
    private Product product;

    @Column(name = "count")
    private int count;

    public UserProduct(){
        // hibernate deed it
    }

    public UserProductId getId() {
        return id;
    }

    public void setId(UserProductId id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public UserProduct(User user, Product product, int count) {
        // create primary key
        this.id = new UserProductId(user.getId(), product.getId());

        // initialize attributes
        this.user = user;
        this.product = product;
        this.count = count;

        // update relationships to assure referential integrity
        product.getUsers().add(this);
        user.getProducts().add(this);
    }

    @Embeddable
    public static class UserProductId implements Serializable {

        @Column(name = "fk_user")
        protected Long userId;

        @Column(name = "fk_product")
        protected Long productId;

        public UserProductId() {

        }

        public UserProductId(Long userId, Long productId) {
            this.userId = userId;
            this.productId = productId;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + ((userId == null) ? 0 : userId.hashCode());
            result = prime * result
                    + ((productId == null) ? 0 : productId.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;

            UserProductId other = (UserProductId) obj;

            if (userId == null) {
                if (other.userId != null)
                    return false;
            } else if (!userId.equals(other.userId))
                return false;

            if (productId == null) {
                if (other.productId != null)
                    return false;
            } else if (!productId.equals(other.productId))
                return false;

            return true;
        }
    }
}
