package persistence;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DataAccess {
    public final SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

}
