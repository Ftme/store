package persistence;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProductPersister extends DataAccess {
    ///////////////////////////////////////////////////////////////
    // ‫چهار عمل اصلی!
    ///////////////////////////////////////////////////////////////

    // id is unique so this method should return single user
    public Product select(double id) {
        Product product = null;
        Session session = factory.openSession();
        Transaction tx = null;
        // use try with resource instead of try catch finally
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Product> query = builder.createQuery(Product.class);
            Root<Product> root = query.from(Product.class);
            query.select(root).where(builder.equal(root.get("id"), id));
            product = session.createQuery(query).uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return product;
    }

    public void insert(Product product) {
        Session session = factory.openSession();
        Transaction tx = null;
        // use try with resource instead of try catch finally
        try {
            tx = session.beginTransaction();
            session.save(product);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    // FIXME use criteria update
    public void update(Product product) {
        Session session = factory.openSession();
        Transaction tx = null;
        // use try with resource instead of try catch finally
        try {
            tx = session.beginTransaction();
            session.merge(product);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
