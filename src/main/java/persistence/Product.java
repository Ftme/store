package persistence;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productId")
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @JsonIgnore
    @Column(name = "price", nullable = false)
    private int price;


    @JsonIgnore
    @Column(name = "tax", nullable = false)
    private double tax;

    @JsonIgnore
    @Column(name = "extraPrice", nullable = false)
    private double extraPrice;

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch=FetchType.EAGER)
    private Set<UserProduct> users = new HashSet<UserProduct>();

    @Column(name = "amount",nullable = false)
    private long amount;


    @JsonProperty("price with tax")
    private double onLoad() {
        return price + (price * tax) + (price * extraPrice);
    }

    public Product() {
        // hibernate need this constructor
    }

    public Product(String name, int price, double tax, double extraPrice, long amount) {
        this.name = name;
        this.price = price;
        this.tax = tax;
        this.extraPrice = extraPrice;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(double extraPrice) {
        this.extraPrice = extraPrice;
    }

    public Set<UserProduct> getUsers() {
        return users;
    }

    public void setUsers(Set<UserProduct> users) {
        this.users = users;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != product.id) return false;
        if (price != product.price) return false;
        if (Double.compare(product.tax, tax) != 0) return false;
        if (Double.compare(product.extraPrice, extraPrice) != 0) return false;
        if (amount != product.amount) return false;
        return name != null ? name.equals(product.name) : product.name == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + price;
        temp = Double.doubleToLongBits(tax);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(extraPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (amount ^ (amount >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", tax=" + tax +
                ", extraPrice=" + extraPrice +
                ", amount=" + amount +
                '}';
    }
}
