package persistence;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class UserPersister extends DataAccess {

    ///////////////////////////////////////////////////////////////
    // ‫چهار عمل اصلی!
    ///////////////////////////////////////////////////////////////

    public User select(int id) {
        User user = null;
        Session session = factory.openSession();
        Transaction tx = null;
        // use try with resource instead of try catch finally
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> query = builder.createQuery(User.class);
            Root<User> root = query.from(User.class);
            query.select(root).where(builder.equal(root.get("id"), id));
            user = session.createQuery(query).uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
    public User select(String userName) {
        User user = null;

        Transaction tx = null;
        // use try with resource instead of try catch finally
        try(Session session = factory.openSession();) {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> query = builder.createQuery(User.class);
            Root<User> root = query.from(User.class);
            query.select(root).where(builder.equal(root.get("username"), userName));
            user = session.createQuery(query).uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return user;
    }
    public boolean insert(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        // use try with resource instead of try catch finally
        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        }catch (ConstraintViolationException e1){
            // log
            return false;
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }
}