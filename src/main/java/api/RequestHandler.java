package api;


import cart.ShowCart;
import cart.SubCart;
import persistence.*;
import response.MyServiceResponse;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;


@Singleton
@Path("/store")
public class RequestHandler {
    private static final UserPersister USER_PERSISTER = new UserPersister();
    private static final ProductPersister PRODUCT_PERSISTER = new ProductPersister();
    private static final UserProductPersister USER_PRODUCT_PERSISTER = new UserProductPersister();

    static {
        // FIXME saveAll

        // do somethings here for example add a user
        User user = new User("ftm");
        USER_PERSISTER.insert(user);

        // add product to DB
        // FIXME add more product here
        PRODUCT_PERSISTER.insert(new Product("product 1", 1000, 0.09, 0.01, 6));
    }

    @GET
    @Path("/{getProduct : (?i)getProduct}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getProduct() {
        Product product = PRODUCT_PERSISTER.select(1);
        return Response.status(Response.Status.OK).entity(new MyServiceResponse(true, product)).build();
    }

    @GET
    @Path("/{addToCart : (?i)addToCart}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response addToCart(@QueryParam("user_name") String username, @QueryParam("product_id") double productId, @QueryParam("count") int num) {
        // FIXME check kon count >0
        Product product = PRODUCT_PERSISTER.select(productId);
        User user = USER_PERSISTER.select(username);
        // FIXME user null check
        if (num < 0 || num > product.getAmount()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new MyServiceResponse(false, 401, "INVALID_COUNT")).build();
        }
        UserProduct userProduct = new UserProduct(user, product, num);
        USER_PRODUCT_PERSISTER.insert(userProduct);
        return Response.status(Response.Status.OK).entity(new MyServiceResponse(true, true)).build();
    }

    @GET
    @Path("/{getCart : (?i)getCart}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getCart(@QueryParam("user_name") String username) {
        User user = USER_PERSISTER.select(username);
        if (user != null) {
            Set<UserProduct> products = user.getProducts();
            Iterator<UserProduct> iterator = products.iterator();
            List<SubCart> productList = new ArrayList<>(products.size());
            double price;
            double sum = 0;
            while (iterator.hasNext()) {
                UserProduct next = iterator.next();
                Product currentProduct = next.getProduct();
                price = currentProduct.getPrice() + currentProduct.getTax() * currentProduct.getPrice() + currentProduct.getExtraPrice() * currentProduct.getPrice();
                SubCart subcart = new SubCart(currentProduct.getName(), next.getCount(), price);
                sum = sum + price * next.getCount();
                productList.add(subcart);
            }
            ShowCart cart = new ShowCart(productList, sum);
            return Response.status(Response.Status.OK).entity(new MyServiceResponse(true, cart)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(new MyServiceResponse(false, 401, "USER_NOT_FOUND")).build();
    }

    @GET
    @Path("/{confirmCart : (?i)confirmCart}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    // FIXME synchronized methods are awful
    synchronized public Response confirmCart(@QueryParam("user_name") String username) {
        User user = USER_PERSISTER.select(username);
        if (user != null) {
            Set<UserProduct> products = user.getProducts();
            Iterator<UserProduct> iterator = products.iterator();
            while (iterator.hasNext()) {
                UserProduct next = iterator.next();
                Product currentProduct = PRODUCT_PERSISTER.select(next.getProduct().getId());
                long remainedAmount = currentProduct.getAmount();
                if (remainedAmount < next.getCount()) {
                    return Response.status(Response.Status.BAD_REQUEST).entity(new MyServiceResponse(false, 401, "MOJOODI_TAMOOM_SHOD!")).build();
                }
                currentProduct.setAmount(remainedAmount - next.getCount());
                PRODUCT_PERSISTER.update(currentProduct);
            }
            // FIXME remove items from user-product table
            return Response.status(Response.Status.OK).entity(new MyServiceResponse(true, true)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(new MyServiceResponse(false, 401, "USER_NOT_FOUND")).build();
    }
}




