package api;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import java.io.Closeable;
import java.io.IOException;

public class App extends Server implements Closeable {
    private static final int PORT = 8888;

    public App() {
        super(PORT);

        ResourceConfig config = new ResourceConfig();
        config.packages("api");

        ServletHolder servlet = new ServletHolder(new ServletContainer(config));

        ServletContextHandler context = new ServletContextHandler(this, "/*");
        context.addServlet(servlet, "/*");
    }

    @Override
    public void close() throws IOException {
        try {
            this.stop();
        } catch (Exception e) {
            throw new IOException("Could't stop server", e);
        }

        if (this.isStarted()) {
            destroy();
        }
    }

    public static void main(String args[]) throws Exception {
        try (App app = new App()) {
            app.start();
            app.join();
        }
    }
}
