package cart;

import java.util.List;

public class ShowCart {

    private List<SubCart> productList;
    private double sum;

    public ShowCart(List<SubCart> productList, double sum) {
        this.productList = productList;
        this.sum = sum;
    }

    public List<SubCart> getProductList() {
        return productList;
    }

    public void setProductList(List<SubCart> productList) {
        this.productList = productList;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
