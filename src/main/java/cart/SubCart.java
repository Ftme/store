package cart;

public class SubCart {
    private String name;
    private int count;
    private Double price;

    public SubCart(String name, int count, Double price) {
        this.name = name;
        this.count = count;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public Double getPrice() {
        return price;
    }
}
