package response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.Objects;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyServiceResponse<T> {
    private Boolean ok;
    private Integer errorCode;
    private String description;
    private T result;

    public MyServiceResponse() {
    }

    public MyServiceResponse(Boolean ok, Integer errorCode, String description) {
        this.ok = ok;
        this.errorCode = errorCode;
        this.description = description;
    }

    public MyServiceResponse(Boolean ok, T result) {
        this.ok = ok;
        this.result = result;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "ok=" + ok +
                ", errorCode=" + errorCode +
                ", description='" + description + '\'' +
                ", result=" + result +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        // Auto generated
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MyServiceResponse<?> that = (MyServiceResponse<?>) o;
        return Objects.equals(ok, that.ok) && Objects.equals(errorCode, that.errorCode) &&
                Objects.equals(description, that.description) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        // Auto generated
        return Objects.hash(ok, errorCode, description, result);
    }
}
